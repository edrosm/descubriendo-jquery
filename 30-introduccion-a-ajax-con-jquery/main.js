// Esperar que el DOM esté listo
$(document).ready(function() {
	

	// // Petición simple con 'Ajax'
	// $.ajax('social.html', {
	// 	success: function(respuesta) {
	// 		$('#a').find('.itemColumna').append($(respuesta));
	// 	}
	// });

	// Método acortado 'get'
	$.get('social.html', function(respuesta) {
		$('#a').find('.itemColumna').append($(respuesta));
	});

	// Cargar JSON desde el sevidor 'getJSON'
	$.getJSON('https://randomuser.me/api/?results=500')
		.then(function(respuesta) {
			// Insertar cada avatar del usuario en el DOM
			respuesta.results.forEach(function(persona) {
				$('<img>')
					.attr('src', persona.picture.thumbnail)
					.appendTo('body');
			});
		});


});