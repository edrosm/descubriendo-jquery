// Esperar que el DOM esté listo
$(document).ready(function() {
	
	// Capturar 'hover'
	$('a').hover(function() {
		// hover in -----------------
		// Guardamos el atributo 'title' y el atributo 'data'-fondo en variables
		var $title = $(this).attr('title'),
			$fondo = $(this).data('fondo');

		// Guardamos el valor de '$title' en el atributo 'data'-titulo y eliminamos el 'title' nativo
		$(this).data('titulo', $title).removeAttr('title');

		// Añadimos nuestro propio 'tooltip'
		$('<p class="tooltip"></p>')
			.text($title)
			.appendTo('body')
			// Cambiamos el color segun el atributo 'data'
			.css('background-color', $fondo)
			.fadeIn('slow');
	}, function() {
		// hover out ----------------
		// Reponemos el 'title' nativo
		$(this).attr('title', $(this).data('titulo'));

		// Eliminamos nuestro 'tooltip'
		$('.tooltip').fadeOut('fast');
	}).mousemove(function(e) {
		// Capturamos la posición X e Y del ratón sobre el ¬
		// -> elemento que lanza nuestro tooltip y la guardamos en variables
		var ratonX = e.pageX + 10,
			ratonY = e.pageY + 12;

		// Hacemos que el 'tooltip' se mueva con el puntero
		$('.tooltip').css({
			'top': ratonY,
			'left': ratonX
		});
	});


});