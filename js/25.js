// Esperar que el DOM esté listo
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-weight': '700'
	}));
	// ---------------------------------- fin ---
	
	// Animación con '.animate()'
	$('#contenedor').find('h2').animate({
		'padding-left': '30px',
		'font-size': '50px'
	}, 1000, function() {
		$('.imagenes').children('div').animate({
			'opacity': '0.3'
		}, 800);
	});

});




















































