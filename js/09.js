// Esperar que el DOM esté listo:
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-style': 'italic'
	}));
	// ---------------------------------- fin ---

	var $parent = $('.itemColumna').find('span').parent();
	$parent.css('border', '1px dashed yellow');

	var $children = $('#contenedor').children();
	$children.css('border', '1px dashed cyan');

	var $find = $('.itemColumna').find('p');
	$find.css({
		'transition': 'all 2s',
		'transform': 'rotate(180deg)',
	});

});
