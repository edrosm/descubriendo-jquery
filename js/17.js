// Esperar que el DOM esté listo
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-style': 'italic'
	}));
	// ---------------------------------- fin ---
	
	// Filtrar resultados con jQuery

	// Con un selector
	var $enlaces = $('#contenedor').find('a');
	$enlaces.filter(':even').css('color', 'yellow');
	$enlaces.filter(':odd').css('color', 'deepskyblue');

	// Con una función
	var $img = $('.imagenes').find('div').filter(function(indice, elemento) {
		// elemento === this
		return $(this).find('span').text() === '1';
		// Si el test es true lo retorna y lo asigna a la variable '$img'
	});

	console.log($img.css('background-color', 'black'));

});