// Esperar que el DOM esté listo
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-style': 'italic'
	}));
	// ---------------------------------- fin ---
	
	// 'Event delegation' (delegación de eventos)

	// // 'Event listener' directo
	// $('main').find('a').on('click', function(evt) {
	// 	evt.preventDefault();
	// 	console.log('a clickado');
	// });

	// 'Event listener' delegado
	// Añadiendo el segundo parámetro despues del evento 'click' hacemos que sólo escuche a los 'a' ¬
	// -> que hay dentro de 'main'. en este caso
	$('main').on('click', 'a', function(evt) {
		evt.preventDefault();
		console.log('a clickado');
	});

	var $e = $('<a href="http://www.google.es" target="_blank">Enlace a google</a><br><br>');
	$('#contenedor').find('p').eq(4).css('color', 'goldenrod').prepend($e);
	
});


