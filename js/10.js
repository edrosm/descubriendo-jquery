// Esperar que el DOM esté listo:
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-style': 'italic'
	}));
	// ---------------------------------- fin ---

	// Añadimos dentro de una variable un objeto jQuery '$()'
	var $enlaceBf = $('<a href="#">...enlace añadido con <code>.before()</code></a>');
	var $enlaceAf = $('<a href="#">...enlace añadido con <code>.after()</code></a>');
	// Antes de un elemento con '.before()'
	$('#b').find('p').eq(2).before($enlaceBf.css('color', 'yellow'));
	// Después de un elemento con '.after()'
	$('#a').find('p').eq(0).after($enlaceAf.css('color', 'blue'));

});
