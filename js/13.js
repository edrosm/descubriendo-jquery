// Esperar que el DOM esté listo
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-style': 'italic'
	}));
	// ---------------------------------- fin ---
	
	// Uso de 'this', 
	$('.itemColumna').find('*').on('click', function(evento) {
		// prevenir comportamiento por defecto de ciertos elementos
		evento.preventDefault(); 
		// hay que convertir 'this' en objeto 'jQuery' ( '$(this' ) ¬
		// --> para que funcione cualquier método o función 'jQuery'
		$(this).css('color', 'goldenrod');
	});
	
});


