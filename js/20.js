// Esperar que el DOM esté listo
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-weight': '700'
	}));
	// ---------------------------------- fin ---
	
	// Eventos de teclado con jQuery

	// Evento '.keyup()'
	var $input = $('#miTexto'),
		$parrafo = $('#contenedor').find('p').first();

	$($input).on('keyup', function() {
		$parrafo.text($input.val());
	});

});




















































