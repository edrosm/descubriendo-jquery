// Esperar que el DOM esté listo
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-weight': '700'
	}));
	// ---------------------------------- fin ---
	
	// Como prevenir el comportamiento por defecto con '.preventDefault()'

	// Con formularios: evitamos recargar la página
	$('form').on('submit', function(evt) {
		evt.preventDefault();
		console.log('-- submit!')
	});

	// Con hiperenlaces: evitamos ir al destino especificado en el atributo 'href' 
	$('#enlace').click(function(evt) {
		evt.preventDefault();
		console.log('-- click');
	})
});




















































