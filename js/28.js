// Esperar que el DOM esté listo
$(document).ready(function() {
	
	// Mostramos el contenido del primer panel
	$('.panel__contenido').first().slideDown(400);

	// Event listener
	$('.panel__cabecera').on('click', function(evt) {
		evt.preventDefault();
		$('.panel__contenido').not(this).slideUp(400);
		$(this).siblings('.panel__contenido').slideToggle(400);
	});

});