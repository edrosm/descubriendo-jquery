// Esperar que el DOM esté listo
$(document).ready(function() {
	
	
	// Ejercicio 1: fullscreen slider --------------------
	// ---------------------------------------------------

	// Buscamos los elementos a guardar / seleccionar ----
	var $li = $('li'),
		$imagenes = $li.find('img'),
		activo = 0,
		cantidad = $imagenes.length;

	// Ocultamos todas las imágenes-----------------------
	$li.hide();

	// Eliminar las imágenes como elemento en si y -------
	// añadirlas como fondo del elemento que queremos ----
	$imagenes.each(function(index, imagen) {
		$li.eq(index).css({
			'background-image': 'url(' + $(imagen).attr('src') + ')',
			'background-size': 'cover',
			'background-position': 'center center',
			'position': 'absolute'
		});
		$(imagen).remove();
	});

	// Mostrar la primera imagen -------------------------
	$li.eq(activo).fadeIn(800);

	// Event listeners------------------------------------
	// Botón derecho
	$('#btn-dcha').click(function(evt) {
		evt.preventDefault();
		activo += 1;
		//Preguntar si estamos en la última diapositiva --
		if (activo > cantidad - 1) {
			activo = 0;
		}
		
		mostrarImagen();
	});

	// Botón izquierdo ----------------------------------
	$('#btn-izda').click(function(evt) {
		evt.preventDefault();
		activo -= 1;
		
		if (activo < 0) {
			activo = cantidad - 1;
		}

		mostrarImagen();
	});

	function mostrarImagen() {
		$li.hide().eq(activo).fadeIn(800);
	}
	// ------------------------------------------ fin ---

	
});

















































