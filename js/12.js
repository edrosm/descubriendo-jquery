// Esperar que el DOM esté listo
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-style': 'italic'
	}));
	// ---------------------------------- fin ---
	

	// Escuchar el '<h1>'
	$('main').find('h1').on('click', function() {
		$('.imagen').css('background-color', 'yellow');
	});

	// Escuchar un elemento y pasarle una función que le afecte sólo a el con 'this'
	$('.columna').on('click', function() {
		$(this).css('background-color', 'blue',);
	});
	
});


