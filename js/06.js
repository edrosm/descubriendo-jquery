// Esperar que el DOM esté listo:
// Con Vanilla JavaScript
document.addEventListener('DOMContentLoaded', function() {
	console.log('DOM listo (Vanilla JavaScript)');
});

// Con jQuery
// Con método '.on()'
$(document).on('DOMContentLoaded', function() {
	console.log('DOM Listo. jQuery \'.on()\'');
});

// Con método '.ready()'
$(document).ready(function() {
	console.log('DOM Listo. jQuery \'.ready()\'');

	// Descendientes directos
	$('.imagenes > .uno').fadeOut('slow');

	// Búsqueda de elementos
	$('.itemColumna h3').css('color', 'yellow');

	// Selectores múltiples
	$('h2, strong').slideToggle(2000);

	// Pseudo clases
	$('.itemColumna:nth(1) p:first').css({
		'color': 'blue',
		'font-weight': '700',
	});

});
