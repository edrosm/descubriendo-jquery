// Esperar que el DOM esté listo
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-style': 'italic'
	}));
	// ---------------------------------- fin ---
	
	// Método '.slideToggle()' de jQuery

	var $img = $('.imagenes').children('div');
	var $h2 = $('.itemColumna').find('h2');
	
	// Mostramos todos a la vez
	$h2.slideToggle('slow', function() {
		console.log('Finalizado');
	});

	// Mostramos uno a uno secuencialmente ------
	$('#boton').on('click', function() {
		mostrarEnSecuencia($img, 100);
	})

	function mostrarEnSecuencia($coleccion, tiempo) {
		tiempo = tiempo || 200;
		$coleccion.each(function(indice, elemento) {
			$(elemento).slideToggle(tiempo * (indice + 1));
		});
	}
	// ---------------------------------- fin ---

});