// Esperar que el DOM esté listo
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-style': 'italic'
	}));
	// ---------------------------------- fin ---
	
	// Atributos 'data' de HTML5 en 'jQuery'
	var $enlace = $('#a').find('.datas').last();
	$enlace.data('datos', 'Mi \'nuevo\' dato 3'); // 2 parámetros (setter), estamos 'sobreescribiendo'
	var $valorAttrData = $enlace.data('datos'); // 1 parámetro (getter), estamos 'obteniendo'
	console.log($valorAttrData);

	// Filtrar selección
	var $datos = $('*').filter(function() {
		return $(this).data('datos');
	});

	console.log($datos);
	
});


