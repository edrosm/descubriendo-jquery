// Esperar que el DOM esté listo
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-style': 'italic'
	}));
	// ---------------------------------- fin ---
	
	// Variable cache. 
	// Para evitar que 'jQuery' tenga que realizar la misma búsqueda una y otra vez


	// Creamos una variable y le asignamos como valor la ruta encadenada de métodos
	var $ultimoParrafo = $('#contenedor').find('p').last();
	
	// Y ahora lo sustituimos en 'jQuery'
	// ejemplo 'CSS'
	$ultimoParrafo.css('color', 'yellow');
	
	// ejemplo atributo 'data'
	$ultimoParrafo.data('color', 'deepskyblue');

	// ejemplo '.append()'
	var $A = $('<br><a href="#">enlace creado con <code>.append()</code></a>');
	$ultimoParrafo.append($A);


	// O también podemos utilizar un encadenamiento de métodos o 'Method chaining'
	var $eP = $('<a href="#">enlace creado con <code>.prepend()</code></a><br>');
	$('#contenedor').find('p').last().prev().css('color', 'deepskyblue').data('font-style', 'italic').prepend($eP);
	
});


