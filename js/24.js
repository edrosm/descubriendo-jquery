// Esperar que el DOM esté listo
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-weight': '700'
	}));
	// ---------------------------------- fin ---
	
	// Manipulación de las clases CSS

	var $h2 = $('#contenedor').find('h2');
	var $pResaltado = $('#contenedor').find('p');
	var $conmutador = $('.imagenes').children();

	
	// '.addClass()'
	$($h2).addClass('h2-azul'); // No se pone el punto propio de la clase

	
	// '.hasClass()', '.removeClass()' y '.addClass()'
	$($conmutador).filter('.uno').click(function() {
		if ($pResaltado.hasClass('p-pistacho')) {
			// Si la clase está aplicada --> la eliminamos
			$($pResaltado).removeClass('p-pistacho');
		} else {
			// Si la clase no está aplicada --> la aplicamos
			$($pResaltado).addClass('p-pistacho');
		}
	});


	// '.toggleClass()'
	$($conmutador).filter('.dos').click(function() {
		$($pResaltado).toggleClass('p-cursiva');
	});

});




















































