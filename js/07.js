// Esperar que el DOM esté listo:
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-style': 'italic'
	}));
	// ---------------------------------- fin ---

	// Entramos en el DOM por la clase 'itemColumna',
	// Usamos '.find()' para buscar los elementos 'p',
	// Y seleccionamos el primero de TODOS los elementos 'p' con '.first()'
	$('.itemColumna').find('p').first().css('color', 'yellow');

	// O seleccionamos el último de TODOS los elementos 'p' con '.last()'
	$('.itemColumna').find('p').last().css('color', 'orange');
	
	// También podemos seleccionar un elemento por su índice con '.eq()'
	$('.itemColumna').find('p').eq(4).css('color', 'blue');

});
