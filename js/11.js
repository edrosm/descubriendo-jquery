// Esperar que el DOM esté listo:
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-style': 'italic'
	}));
	// ---------------------------------- fin ---

	// Dentro de un elemento, al principio del propio elemento '.prepend()'
	var $prepend = $('<p>...dentro, al principio con <code>.prepend()</code></p>');
	$('.lista').find('li').last().prepend($prepend.css('color', 'yellow'));

	// Dentro de un elemento, al final del propio elemento '.append()'
	var $append = $('<p>...dentro, al final con <code>.append()</code></p>');
	$('.lista').find('li').last().append($append.css('color', 'blue'));

	// Eliminar el elemento seleccionado
	$('.imagen').last().remove();

});
