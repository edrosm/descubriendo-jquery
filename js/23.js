// Esperar que el DOM esté listo
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-weight': '700'
	}));
	// ---------------------------------- fin ---
	
	// Manipulación de CSS con el método '.css()'

	// 'getter' simple (obtener el valor de una propiedad)
	console.log($('#a').css('font-family'));

	// 'getter' múltiple (obtener el valor de varias propiedades a la vez)
	console.log($('#b').css([
		'font-family',
		'font-weight',
		'font-style',
		'color'
	]));

	// 'setter' simple (cambiar el valor de una propiedad)
	var $parrafo = $('#contenedor').find('p');
	$parrafo.first().css('color', 'yellow');

	// 'setter' múltiple (cambiar el valor de varias propiedades a la vez)
	$parrafo.last().css({
		'font-family': 'Monaco',
		'font-size': '12px',
		'font-style': 'bold',
		'color': 'deepskyblue'
	});


});




















































