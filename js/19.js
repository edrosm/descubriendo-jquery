// Esperar que el DOM esté listo
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-weight': '700'
	}));
	// ---------------------------------- fin ---
	
	// Eventos de ratón con jQuery
	//-------------------------------------------

	// Le doy un poco de estilo a los botones
	var $botones = $('.botones').children('div');

	$botones.css({
		'width': '100px',
		'display': 'block',
		'padding': '5px',
		'margin-top': '4px',
		'border': '1px solid #fff',
		'border-radius': '8px',
		'color': '#fff',
		'text-align': 'center',
		'cursor': 'pointer'
	});


	// --- Evento '.click()' ---
	$botones.filter('.click').click(function() {
		$(this).css({
			'border': '1px solid deepskyblue',
			'color': 'deepskyblue'
		})
		$(this).text('click');
	});

	// --- Evento '.dblclick()' ---
	$botones.filter('.click').dblclick(function() {
		$(this).css({
			'border': '1px solid yellow',
			'color': 'yellow'
		});
		$(this).text('double click');
	});

	// --- Evento '.contextmenu()' ---
	$botones.filter('.contextmenu').contextmenu(function() {
		$('h2').css('font-size', '40px');
	});

	// --- Evento '.hover()' ---
	var $imagenes = $('.imagenes').children();

	$botones.filter('.hover').hover(function() {
		$imagenes.filter(':even').css('background-color', 'blue');
	}, function() {
		$imagenes.filter(':odd').css('background-color', 'yellow');
	});

});




















































