// Esperar que el DOM esté listo
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-weight': '700'
	}));
	// ---------------------------------- fin ---
	
	// Animación con clases CSS
	var $main = $('main');

	$main.addClass('animated bounceIn');

	var timeout = setTimeout(function() {
		$main.addClass('bounceOut');
		// Limpiamos el 'time out' de la memoria
		clearTimeout(timeout);
	}, 2500);

});




















































