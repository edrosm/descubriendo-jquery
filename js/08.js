// Esperar que el DOM esté listo:
$(document).ready(function() {
	
	// --- Mostrar el 'DOM listo' ---------------
	var $console = $('<code>DOM Listo...</code>');
	$('main').find('h1').after($console.css({
		'color': 'blue',
		'padding': '5px 0 0 5px',
		'font-style': 'italic'
	}));
	// ---------------------------------- fin ---

	$('#contenedor').find('p').css('color', 'yellow');
	$('#contenedor').find('p').last().prev().css('background-color', 'black');
	$('#contenedor').find('h2').css('color', 'blue');
	$('#contenedor').find('h2').last().css('color', 'red');
	$('#contenedor').find('p').last().prev().next().css('color', 'blue');

});
