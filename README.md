# descubriendo-_jQuery_

Repositorio de _**jQuery**_ basado en la serie de *screencast* de Juan Andrés Núñez: _«Aprende **jQuery** de una vez por todas»_.
---

### Screencasts y ejercicios:
1. Primeros pasos
2. Manipular el **DOM** sin _jQuery_
3. Seleccionar elementos HTML sin _jQuery_
4. Manipular las clases de un elemento sin _jQuery_
5. Manipular los estilos de un elemento sin _jQuery_
6. Buscar elementos en el **DOM** con _jQuery_
7. Búsqueda y filtrado en el **DOM** con _jQuery_
8. Encadenar métodos y recorrer el **DOM** con _jQuery_
9. Buscar arriba y abajo en el **DOM** con _jQuery_
10. Insertar en el **DOM** con _jQuery_ (1)
11. Insertar en el **DOM** con _jQuery_ (2)
12. Interacción en el **DOM**
13. `This` y _jQuery_
14. Atributos `data` con _jQuery_
15. _Variable cache_
16. _Event delegation_ con _jQuery_
17. Filtrar resultados con _jQuery_
18. Método `.slideToggle()` de _jQuery_ 
19. Eventos de ratón con _jQuery_
20. Eventos de teclado con _jQuery_
21. Keyboard keycodes con _jQuery_
22. Prevenir comportamiento por defecto con _jQuery_
23. Manipula **CSS** con _jQuery_
24. Manipula las clases **CSS** con _jQuery_
25. Animación con _jQuery_
26. Animación con clases **CSS** _(animate.css)_ en _jQuery_
27. Ejercicio 1: fullscreen slider con _jQuery_
28. Ejercicio 2: acordeón con _jQuery_
29. Ejercicio 3: tooltip con _jQuery_
30. Introducción de **AJAX** con _jQuery_
31. Opciones de **AJAX** con _jQuery_
32. ...