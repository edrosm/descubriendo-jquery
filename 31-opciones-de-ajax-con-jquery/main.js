// Esperar que el DOM esté listo
$(document).ready(function() {
	
	// Petición esencial con Ajax
	$.ajax('social.html', {
		success: function(respuesta) {
			$('#c').find('.itemColumna').append($(respuesta));
		},
		error: function(request, tipoError, mensaje) {
			console.error(arguments);
		},
		complete: function(request, status) {
			console.info(arguments);
		},
		beforeSend: function() {
			// Mostrar loading spinner
		},
		timeout: 1000,
	});

	// En jQuery 3.0 > = Promise API
	$.ajax('texto.html')
		// success()
		.done(function(respuesta) {
			$('#b').find('.itemColumna').append($(respuesta));
		})
		// error()
		.fail(function(request, tipoError, mensaje) {
			console.error(arguments);
		})
		// complete()
		.always(function(request, status) {
			console.info(arguments);
		})

});